<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::middleware(['web'])
    ->namespace('App\Http\Controllers\Tenants')
    ->as('tenant.')
    ->group(function () {
        Route::resource('/home','CatalogueController');
        Route::resource('/products', 'ProductsController')->middleware('role:admin');
        Route::resource('/products/{id}/edit', 'ProductsController')->middleware('role:admin');
        Route::resource('/orders', 'OrdersController');
        Route::resource('/orders/{id}/edit', 'OrdersController');
        Route::resource('/catalogue', 'CatalogueController');
        Route::get('/catalogue-order/{id}', 'CatalogueController@catalogueOrder')->name('catalogueOrder');
        Route::resource('/home-admin','HomeAdminController')->middleware('role:admin');
        Route::resource('/reports', 'ReportsController')->middleware('role:admin');
        Route::get('/report-selled', 'ReportsController@reportSelled')->name('reportSelled');
        Route::get('/report-purchases', 'ReportsController@reportPurchases')->name('reportPurchases');
        Route::get('/report-users', 'ReportsController@reportUsers')->name('reportUsers');
        Route::resource('/user', 'UserController');
        Route::resource('/user/{id}/edit', 'UserController');
    });
