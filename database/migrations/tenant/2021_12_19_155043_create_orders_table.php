<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    protected $system = true;

    public function up()
    {
        //
        Schema::create('orders', function (Blueprint $table) {
    
            $table->bigIncrements('id');
            $table->string('quantity');
            $table->string('total_price');
            $table->boolean('paid')->default(false);


            $table->bigInteger('id_product')->unsigned()->nullable();
            $table->bigInteger('id_client')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_product')->references('id')->on('products')->onDelete('set null');
            $table->foreign('id_client')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}