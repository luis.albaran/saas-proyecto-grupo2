<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Tenant\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        "name" => $faker->name,
        "price" => $faker->randomFloat($nbMaxDecimals = 2, $min = 5, $max = 1000),
        'description' => $faker->paragraph,
        'stars' => $faker ->numberBetween($min = 2, $max = 5),
        "created_at" => now(),
        "quantity" => $faker ->numberBetween($min = 10, $max = 100),
        "url_img" => $faker-> imageUrl($width = 640, $height = 480)
    ];
});
