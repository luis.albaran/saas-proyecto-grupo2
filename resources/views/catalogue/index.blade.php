@extends('layouts.app')
@section('content')
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>Jumbotron Template · Bootstrap v4.6</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.6/examples/jumbotron/">


    <!-- Bootstrap core CSS -->
    <link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
        .text-gray {
            color: #aaa
        }

        .img-product {
            height: 170px;
            width: 240px;
            border-radius: 8px;
        }
        .padding-img{
            padding-left: 20px;
            padding-right: 20px;
            padding-top: 40px;
            padding-bottom: 40px;
        }
        .tag-stock{
            background-color: #f5c842;
            color: black;
            border-radius: 3px;
            padding-right: 3px;
            padding-left: 3px;
            font-weight: bold;
        }
        .tag-no-stock{
            background-color: gray;
            color: white;
            border-radius: 3px;
            padding-right: 3px;
            padding-left: 3px;
        }
        .star-true{
            color: #f5c842;
        }
        .button {
            border: none;
            background-color:#47afb3;
            color: #FFFFFF;
            text-align: center;
            width: 200px;
            transition: all 0.2s;
            cursor: pointer;
            }

            .button span {
            cursor: pointer;
            display: inline-block;
            position: relative;
            transition: 0.2s;
            }

            .button span:after {
            content: '\00bb';
            position: absolute;
            opacity: 0;
            top: 0;
            right: -20px;
            transition: 0.2s;
            }

            .button:hover span {
            padding-right: 25px;
            }

            .button:hover span:after {
            opacity: 1;
            right: 0;
            }
            .footer{
                background-color: gray;
                margin-top:20px;
            }
    </style>

    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
</head>

<body class="">
    <div class="container ">
        <div class="col-lg-8 mx-auto">
            <h4 class="font-weight-bold">Catalogue</h4>
            <!-- List group-->
            @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}  
            </div><br />
            @endif
            <ul class="list-group shadow">
                <!-- list group item-->
                @foreach($catalogue as $product)
                @php
                $source_image = "https://picsum.photos/200/300";
                @endphp
                <li class="list-group-item">
                    <!-- Custom content-->
                    <div class="media align-items-lg-center flex-column flex-lg-row p-3">
                        <div class="media-body order-2 order-lg-1">
                            <h5 class="mt-0 font-weight-bold mb-2">{{$product->name}}</h5>
                            <p class="font-italic text-muted mb-0 small">{{$product->description}}</p>
                            <div class="d-flex align-items-center justify-content-between mt-1">
                                <h6 class="font-weight-bold my-2">${{$product->price}}</h6>
                                <ul class="list-inline small">
                                    @for ($i = 1; $i <= 5; $i++)
                                        @if($i <= $product->stars)
                                        <li class="list-inline-item m-0"><i class="fa fa-star star-true"></i></li>
                                        @else
                                        <li class="list-inline-item m-0"><i class="fa fa-star text-gray"></i></li>
                                        @endif
                                    @endfor
                                </ul>
                            </div>
                            @if($product->quantity > 0)
                            <span class="tag-stock">In stock</span>
                            <div class="d-flex align-items-center justify-content-between mt-1">
                                <form method="POST" action="/catalogue/{{$product->id}}">
                                {{ csrf_field() }}
                                {{ method_field('POST') }}
                                <a  href="{{ url('catalogue-order/'.$product->id) }}" class="btn btn-cart button text-black btn-md font-weight-bold" 
                                disabled><span>{{ __('Add to cart') }}</span></a>
                            </div>
                            @else
                            
                            <span class="tag-no-stock">Out to stock</span>
                            <div class="d-flex align-items-center justify-content-between mt-1">
                                <button class="btn btn-cart btn-md font-weight-bold" disabled>{{ __('Add to cart') }}</button>
                            </div>
                            @endif
                        </div>
                        <!--<img src="https://picsum.photos/200" alt="Product img" width="200" class="img-product ml-lg-5 order-1 order-lg-2">-->
                        <div class="card ml-lg-5 order-1 order-lg-2 padding-img">IMG Article {{$product->id}}</div>
                    </div> <!-- End -->
                </li>
                @endforeach
            </ul>
        </div>
    </body>


@endsection