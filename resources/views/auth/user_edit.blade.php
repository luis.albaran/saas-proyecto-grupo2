@extends('layouts.app')

@section('content')
<style>
    .uper {
        margin-top: 30px;
    }
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card text-muted bg-primary mb-3">
                <div class="card uper">
                    <div class="card-header">
                        <h4 class="font-weight-bold">Update profile</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                        @endif
                        <form method="POST" action="{{ url('/user', $user->id) }}">
                            @if(session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}  
                            </div><br />
                            @endif
                            <div class="form-group">
                                @csrf
                                @method('PUT')
                                <label for="name">Name:</label>
                                <input type="text" class="form-control" name="name" value="{{$user->name}}" />
                            </div>
                            <div class="form-group">
                                <label for="price">Email:</label>
                                <input type="text" class="form-control" name="email" value="{{$user->email}}" />
                            </div>
                            <div class="form-group">
                                <label for="price">Password:</label>
                                <input type="password" class="form-control" name="password" value="" />
                            </div>
                            <!-- <button type="submit" class="btn btn-secondary">Cancel</button> -->
                            <a class="btn btn-secondary" href="{{ url('/home') }}">Cancel</a>
                            <button type="submit" class="btn btn-primary">Update data</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection