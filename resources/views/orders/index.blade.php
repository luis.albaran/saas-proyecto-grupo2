@extends('layouts.app')

<head>
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
</head>
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-7">
                        <h4 class="font-weight-bold"><i class="fas fa-shopping-cart"></i>{{ __(' Cart') }}</h4>
                        </div>
                        <div class="col-md-2">
                        <p><a class="btn btn-secondary" href="{{ url('/catalogue') }}" role="button"> <i class="fas fa-store">Catalogue</i></a></p>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}  
                        </div><br />
                    @endif

                    <table class="table table-hover">
                        <thead class="">
                            <tr>
                            <td>Name Producto</td>
                            <td>Quantity</td>
                            <td>Total Price</td>
                            <td>Pay</td>
                            <td class="text-center">Actions</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orders as $order)
                            <tr>
                            @if (Auth::user()->id  ==  $order->id_client && $order->paid == 0)
                                <td>{{$order->nameProduct}}</td>
                                <td>{{$order->quantity}}</td>
                                <td>{{$order->total_price}}</td>
                                @if ($order->paid == 0)
                                    <td>Por pagar</td>
                                @else
                                    <td>Pagado</td>
                                @endif
                                <!--<td>{{$order->paid}}</td> -->
                            
                                <td>
                                    <form action="{{ url('/orders', $order->id)}}" method="post" class="row mx-1">     
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-block col-sm">Delete</button>
                                    </form>
                                </td>
                            @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div>
                            <!-- echo "<div class>"$suma</div> -->
                    </div>
                    <div class="text-center">
                    <form method="POST" action="{{ url('/orders', $orders) }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <button type="submit" class="btn btn-success"> Buy </button>
                   </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection