@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                <div class="row">
                        <div class="col-md-7">
                        <h4 class="font-weight-bold">{{ __('Consult, Edit and Delete products') }}</h4>
                        </div>
                        <div class="col-md-3">
                        <a class="btn btn-primary btn-block text-white" href="{{ url('/products/create') }}" role="button">Añadir productos &raquo;</a>
                        </div>
                        <div class="col-md-2">
                        <p><a class="btn btn-secondary" href="{{ url('/home') }}" role="button">Back &raquo;</a></p>
                        </div>
                        

                    </div>
                </div>
                <form action="{{ url('/products')}}" method="get" >
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-7 mx-2" >
                                <input type="text" class="form-control" name="busqueda">
                            </div>
                            <div class="col-sm-4 mx-2">
                                <button  type="submit" class="btn btn-outline-primary btn-block">Buscar Producto</button>
                                
                            </div>
                        </div>
                    </div>
                </form>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-hover">
                        <thead class="">
                            <tr>
                            <td>ID</td>
                            <td>Product Name</td>
                            <td>Price</td>
                            <td>Created At</td>
                            <td class="text-center">Actions</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $product)
                            <tr>
                                <td>{{$product->id}}</td>
                                <td>{{$product->name}}</td>
                                <td>{{$product->price}}</td>
                                <td>{{$product->created_at}}</td>

                                <td>
                                    <form action="{{ url('/products', $product->id)}}" method="post" class="row mx-1">
                                    <a href="{{ url('/products/' .$product->id .'/edit',  ) }}" class="col-sm btn btn-outline-secondary btn-block mr-1">
                                        Edit
                                    </a>         
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-block col-sm">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
