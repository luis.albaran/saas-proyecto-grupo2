@extends('layouts.app')

@section('content')
<style>
    .uper {
        margin-top: 30px;
    }
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card text-muted bg-primary mb-3">
                <div class="card uper">
                    <div class="card-header">
                        <h4 class="font-weight-bold">Update products</h4> 
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                        @endif
                        <form method="POST" action="{{ url('/products', $product->id) }}">
                            <div class="form-group">
                                @csrf
                                @method('PUT')
                                <label for="name">Product Name:</label>
                                <input type="text" class="form-control" name="name" value="{{$product->name}}" />
                            </div>
                            <div class="form-group">
                                <label for="price">Price:</label>
                                <input type="text" class="form-control" name="price" value="{{$product->price}}" />
                            </div>
                            <!-- <button type="submit" class="btn btn-secondary">Cancel</button> -->
                            <a class="btn btn-secondary" href="{{ url('/products') }}">Cancel</a>
                            <button type="submit" class="btn btn-primary">Update Product</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection