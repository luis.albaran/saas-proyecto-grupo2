@extends('layouts.app')
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</head>

@section('content')
<style>
    .uper {
        margin-top: 30px;
    }
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card text-muted bg-primary mb-3">
                <div class="card uper">
                    <div class="card-header">
                        <h4 class="font-weight-bold">Reports</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                        @endif
                        <form  action="{{ url('/reports') }}" method="get">

                            <div class="form-group">
                                
                                <label for="initial_date">{{__('Initial date :')}}</label>
                                <input id="initial_date" type="date" class="form-control" name="initial_date" value="{{ old('initial_date') }}" required autocomplete="initial_date" autofocus />
                            </div>
                            <div class="form-group">
                                <label for="end_date">{{__('End date :')}}</label>
                                <input id="end_date" type="date" class="form-control" name="end_date" value="{{ old('end_date') }}" required autocomplete="end_date" autofocus />

                            </div>
                            <!-- <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="submit" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                    Generate report
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li><a class="dropdown-item" href="{{url('/report-selled')}}"><span>{{__('10 most selled products')}}</span></a></li>
                                    <li><a class="dropdown-item" href="{{url('/report-purchases')}}"><span>{{__('Top 10 customers with more purchases')}}</span></a></li>
                                    <li><a class="dropdown-item" href="{{url('/report-users/')}}"><span>{{__('Last 10 users registered')}}</span></a></li>
                                </ul>
                            </div> -->
                            <button class="btn btn-primary" type="submit" id="dropdownMenuButton1" >
                                    Generate report
                            </button>
                        </form>
                        @if(!empty($orders))
                        <table class="table table-hover">
                        <thead class="">
                            <tr>
                            <td>ID</td>
                            <td>Product Name</td>
                            <td>Total Price</td>
                            <td>Name Client</td>
                            <td>Date</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orders as $order)
                            <tr>
                            @if ($order->paid == 1)
                            <td>{{$order->id}}</td>
                                <td>{{$order->nameProduct}}</td>
                                <td>{{$order->total_price}}</td>
                                <td>{{$order->nameClient}}</td>
                                <td>{{$order->created_at}}</td>

                            @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection