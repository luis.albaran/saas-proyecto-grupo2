@extends('layouts.app')

@section('content')
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>Jumbotron Template · Bootstrap v4.6</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.6/examples/jumbotron/">


    <!-- Bootstrap core CSS -->
    <link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .card-radius {
            border-radius: 10px;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>

    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">
</head>

<body>
    <main role="main">
        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
            <div class="container">
                <h1 class="display-4">Bienvenid@ {{ Auth::user()->name }}!</h1>
                <p></p>
            </div>
        </div>

        <div class="container">
            <div class="card card-radius  py-2">
                <div class="card-body">
                    <h1 class="card-title text-bold font-weight-bold">Productos</h1>
                    <div class="row py-2">
                        <div class="col-md-3">
                            <h4>Consultar</h4>
                            <p class="">Aquí podrás consultar todos los productos registrados en tu inventario con información como nombre, precio y cuándo se añadió el producto.</p>
                        </div>
                        <div class="col-md-3">
                            <h4>Añadir</h4>
                            <p>Aquí podrás añadir los productos que recien llegan a tu inventario, específicando el tipo de producto y su precio.</p>
                        </div>
                        <div class="col-md-3">
                            <h4>Eliminar</h4>
                            <p>Aquí podrás eliminar cualquier producto que ya no esté en tu inventario, buscando en en listado.</p>
                        </div>
                        <div class="col-md-3">
                            <h4>Otras consultas</h4>
                            <p>Aquí podrás visualizar el top de productos más vendidos, top de clientes y los últimos usuarios registrados.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <p><a class="btn btn-primary btn-block text-white" href="{{ url('/products') }}" role="button">Consultar productos &raquo;</a></p>
                        </div>
                        <div class="col-md-3">
                            <p><a class="btn btn-primary btn-block text-white" href="{{ url('/products/create') }}" role="button">Añadir productos &raquo;</a></p>
                        </div>
                        <div class="col-md-3">
                            <p><a class="btn btn-primary btn-block text-white" href="{{ url('/products') }}" role="button">Elminar productos &raquo;</a></p>
                        </div>
                        <div class="col-md-3">
                            <p><a class="btn btn-primary btn-block text-white" href="{{ url('/reports') }}" role="button">Otras consultas&raquo;</a></p>
                        </div>
                    </div>

                </div>
            </div>      
            <hr>
        </div> <!-- /container -->

    </main>

    <footer class="container">
        <p></p>
    </footer>


    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script>
        window.jQuery || document.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')
    </script>
    <script src="../assets/dist/js/bootstrap.bundle.min.js"></script>


</body>

</html>

@endsection
