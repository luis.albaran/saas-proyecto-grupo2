<?php

namespace App\Http\Controllers\Tenants;
use App\Models\Tenant\Orders;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }



     public function index(Request $request){
        //$orders = Orders::all();
        $orders = Orders::select('users.name as nameClient',
            'products.name as nameProduct','orders.id', 'orders.id_client',
            'orders.quantity', 'orders.total_price',
            'orders.paid','orders.created_at')
            ->join('users', 'users.id','=', 'orders.id_client')
            ->join('products', 'products.id','=', 'orders.id_product')
            ->get();
        $id_client = auth()->user()->id;
        $suma = 0;
        foreach($orders as $order)
            {
                if ($order->id_client == $id_client){
                        $suma += $order->paid;
                }
            }
        
        return view('orders.index', compact('orders', 'suma'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        // $products = Product::where("price","=",5000)->select("id","name","price");
        print_r($id);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }


    public function update(Request $request, $orders)
    {   
        $id_client = auth()->user()->id;
        $orders = Orders::all();
        if (is_array($orders) || is_object($orders))
        {
            foreach($orders as $order)
            {
                if ($order->id_client == $id_client){
                        $order->paid = 1;
                        $order->save();
                }
            }
        return redirect('/orders')->with('success', 'The product was purchased successfully');
        }
        
         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $orders = Orders::findOrFail($id);
        $orders->delete();

      return redirect('/orders')->with('success', 'order is successfully delted');
    }
}
