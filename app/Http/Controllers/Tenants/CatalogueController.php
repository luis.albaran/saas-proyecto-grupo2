<?php

namespace App\Http\Controllers\Tenants;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tenant\Product;
use App\Models\Tenant\Orders;
use Auth;

class CatalogueController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $catalogue = Product::all();
        return view('catalogue.index', compact('catalogue'));
    }

    public function catalogueOrder(Request $request, $id) {
        echo $id;
        $product = Product::find($id);
        $order = [
            'quantity' => 1,
            'total_price' => $product->price,
            'id_product' => $id,
            'id_client' => auth()->user()->id,
        ];
        Orders::create($order);
        $product->quantity = $product->quantity - 1;
        $product->save();

        return redirect('/home')->with('success', 'Order was successfully added to cart');
    }

}
