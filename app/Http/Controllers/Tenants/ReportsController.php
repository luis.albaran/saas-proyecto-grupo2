<?php

namespace App\Http\Controllers\Tenants;

use App\Models\Tenant\Orders;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tenant\Product;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request){
        
        // $orders = Orders::orderBy('quantity')
        //     // 'products.name as nameProduct','orders.id', 
        //     // 'orders.quantity', 'orders.total_price',
        //     // 'orders.paid','orders.created_at')
        //     // ->join('users', 'users.id','=', 'orders.id_client')
        //     // ->join('products', 'products.id','=', 'orders.id_product')
        //     // ->groupBy('orders.total_price')
        //     ->having("quantity",">",1)
        //     ->get();
        // print_r($orders);

        if($request->get('initial_date')){
            $orders = Orders::select('users.name as nameClient',
            'products.name as nameProduct','orders.id', 
            'orders.quantity', 'orders.total_price',
            'orders.paid','orders.created_at')
            ->join('users', 'users.id','=', 'orders.id_client')
            ->join('products', 'products.id','=', 'orders.id_product')
            ->get();
            return view('reports.index', compact('orders'));

        }else{
            $orders = [];
        return view('reports.index', compact('orders'));

        }

    }

    public function reportSelled(Request $request) {
        
        $orders = Orders::select('users.name as nameClient',
            'products.name as nameProduct','orders.id', 
            'orders.quantity', 'orders.total_price',
            'orders.paid','orders.created_at')
            ->join('users', 'users.id','=', 'orders.id_client')
            ->join('products', 'products.id','=', 'orders.id_product')
            ->havingRaw('Count(*) > 1')
            ->get();

        return view('reports.index', compact('orders'));
      

        //return view('products.edit')->with('product',$product);

        //return redirect('/home')->with('success', 'Order was successfully added to cart');
    }

    public function reportPurchases(Request $request) {
        print_r("veaaa2");
        
        print_r($request->get('initial_date'));
        print_r($request->get('end_date'));
        //return redirect('/home')->with('success', 'Order was successfully added to cart');
    }

    public function reportUsers(Request $request) {
        print_r("veaaa3");
        
        print_r($request->get('initial_date'));
        print_r($request->get('end_date'));
        //return redirect('/home')->with('success', 'Order was successfully added to cart');
    }
}
